/**
 *
 */
package org.swamy.exercise.wlcounter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.swamy.exercise.wlcounter.biz.CountsCalculator;
import org.swamy.exercise.wlcounter.data.AlphabetCount;
import org.swamy.exercise.wlcounter.data.SentenceWordCountResult;
import org.swamy.exercise.wlcounter.data.UserInput;

@RestController
public class WLCounterService {

	@Autowired
	CountsCalculator countsCalculator;

	@PostMapping("/word_count_per_sentence")
	public @ResponseBody SentenceWordCountResult countWordsInSentence(@RequestBody final UserInput userInput)
			throws Exception {

		if (null == userInput || null == userInput.inputText || userInput.inputText.trim().equals("")) {
			throw new Exception("No input text supplied.");
		}

		final SentenceWordCountResult result = new SentenceWordCountResult();
		result.getSentenceWordCountList().addAll(countsCalculator.countWordsInSentencesForText(userInput.inputText));

		return result;
	}

	@PostMapping("/total_letter_count")
	public @ResponseBody List<AlphabetCount> countLettersByAlphabet(@RequestBody final UserInput userInput)
			throws Exception {
		if (null == userInput || null == userInput.inputText || userInput.inputText.trim().equals("")) {
			throw new Exception("No input text supplied.");
		}

		return countsCalculator.countAlphabetsInText(userInput.inputText);
	}
}
