package org.swamy.exercise.wlcounter.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest
public class WLCounterServiceTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testCountWordsInSentence() throws Exception {
		mockMvc.perform(post("http://localhost:8080/word_count_per_sentence")
				.content("{\"inputText\":\"this is a test sentence\"}").contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
		// .andExpect(content().string(containsString("\"wordCount\": 5")));
	}

	@Test
	public void testCountLettersByAlphabet() throws Exception {
		mockMvc.perform(
				post("http://localhost:8080/total_letter_count").content("{\"inputText\":\"this is a test sentence\"}")
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

}
