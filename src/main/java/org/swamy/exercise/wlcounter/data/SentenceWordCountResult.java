package org.swamy.exercise.wlcounter.data;

import java.util.ArrayList;
import java.util.List;

public class SentenceWordCountResult {
	List<SentenceWordCount> sentenceWordCountList = new ArrayList<>();

	public List<SentenceWordCount> getSentenceWordCountList() {
		return sentenceWordCountList;
	}
}
