package org.swamy.exercise.wlcounter.biz;

import static org.springframework.util.CollectionUtils.isEmpty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.swamy.exercise.wlcounter.data.AlphabetCount;
import org.swamy.exercise.wlcounter.data.SentenceWordCount;

@Service
public class CountsCalculator {

	public List<SentenceWordCount> countWordsInSentencesForText(final String inputText) {

		if (null == inputText || inputText.trim().equals("")) {
			throw new RuntimeException("Input text cannot be null or empty");
		}

		final List<SentenceWordCount> sentenceWordCountList = new ArrayList<>();
		final List<String> sentencesInText = getSentencesFromText(inputText);
		if (!isEmpty(sentencesInText)) {
			sentencesInText.forEach(sentence -> {
				final SentenceWordCount wordCountInSentence = countWordsInSentence(sentence);
				sentenceWordCountList.add(wordCountInSentence);
			});
		}

		return sentenceWordCountList;
	}

	protected SentenceWordCount countWordsInSentence(final String sentence) {

		if (null == sentence || sentence.trim().equals("")) {
			throw new RuntimeException("Input text cannot be null or empty");
		}

		final String[] words = sentence.trim().split("[\\s]+");
		final SentenceWordCount wordCountInSentence = new SentenceWordCount();
		wordCountInSentence.setSentence(sentence);
		if (null != words) {
			wordCountInSentence.setWordCount(words.length);
		}
		return wordCountInSentence;
	}

	protected List<String> getSentencesFromText(final String inputText) {

		if (null == inputText || inputText.trim().equals("")) {
			throw new RuntimeException("Input text cannot be null or empty");
		}

		final String[] sentences = inputText.trim().split("[.\\\\?!]+");
		return sentences != null && sentences.length > 0 ? Arrays.asList(sentences) : new ArrayList<>();
	}

	public List<AlphabetCount> countAlphabetsInText(final String inputText) {

		if (null == inputText || inputText.trim().equals("")) {
			throw new RuntimeException("Input text cannot be null or empty");
		}

		final List<AlphabetCount> alphabetCountList = new ArrayList<>();
		final Map<Character, Integer> alphabetCountsMap = new HashMap<Character, Integer>();

		final char[] inputChars = inputText.toCharArray();
		for (int i = 0; i < inputChars.length; i++) {
			int alphabetCount = 0;
			if (Character.isAlphabetic(inputChars[i])) {
				if (alphabetCountsMap.containsKey(Character.toUpperCase(inputChars[i]))) {
					alphabetCount = alphabetCountsMap.get(Character.toUpperCase(inputChars[i]));
				}

				alphabetCountsMap.put(Character.toUpperCase(inputChars[i]), alphabetCount + 1);
			}
		}

		alphabetCountsMap.entrySet().forEach(row -> {
			final AlphabetCount alphabetCount = new AlphabetCount();
			alphabetCount.setAlphabet(row.getKey());
			alphabetCount.setAlphabetCount(row.getValue());

			alphabetCountList.add(alphabetCount);
		});

		return alphabetCountList;
	}
}
