package org.swamy.exercise.wlcounter;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.swamy.exercise.wlcounter.controller.WLCounterService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WordLetterCounterApplicationTest {

	@Autowired
	WLCounterService wlCounterService;

	@Test
	public void contextLoads() throws Exception {
		assertNotNull(wlCounterService);
	}

}
