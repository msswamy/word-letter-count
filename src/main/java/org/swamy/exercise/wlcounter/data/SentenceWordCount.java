package org.swamy.exercise.wlcounter.data;

public class SentenceWordCount {
	private String sentence;

	private Integer wordCount = 0;

	public String getSentence() {
		return sentence;
	}

	public void setSentence(final String sentence) {
		this.sentence = sentence;
	}

	public Integer getWordCount() {
		return wordCount;
	}

	public void setWordCount(final Integer wordCount) {
		this.wordCount = wordCount;
	}
}
