package org.swamy.exercise.wlcounter.data;

public class AlphabetCount {
	Character alphabet;

	Integer alphabetCount;

	public Character getAlphabet() {
		return alphabet;
	}

	public void setAlphabet(final Character alphabet) {
		this.alphabet = alphabet;
	}

	public Integer getAlphabetCount() {
		return alphabetCount;
	}

	public void setAlphabetCount(final Integer alphabetCount) {
		this.alphabetCount = alphabetCount;
	}
}
