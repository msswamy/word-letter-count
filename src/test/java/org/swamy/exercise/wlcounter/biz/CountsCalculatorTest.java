package org.swamy.exercise.wlcounter.biz;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.swamy.exercise.wlcounter.data.AlphabetCount;
import org.swamy.exercise.wlcounter.data.SentenceWordCount;

public class CountsCalculatorTest {

	CountsCalculator countsCalculator = new CountsCalculator();

	@Test
	public void testCountWordsInSentencesForText_validSingleSentence() {
		final String inputText = "This is a test sentence.";
		final List<SentenceWordCount> result = countsCalculator.countWordsInSentencesForText(inputText);

		assertNotNull(result);
		assertTrue(result.size() == 1);
		assertTrue(result.get(0).getWordCount() == 5);
	}

	@Test
	public void testCountWordsInSentencesForText_lotsOfWhitespace() {
		final String inputText = "This is     a test sentence      .";
		final List<SentenceWordCount> result = countsCalculator.countWordsInSentencesForText(inputText);

		assertNotNull(result);
		assertTrue(result.size() == 1);
		assertTrue(result.get(0).getWordCount() == 5);
	}

	@Test
	public void testCountWordsInSentencesForText_noDelimiter() {
		final String inputText = "This is     a test sentence     ";
		final List<SentenceWordCount> result = countsCalculator.countWordsInSentencesForText(inputText);

		assertNotNull(result);
		assertTrue(result.size() == 1);
		assertTrue(result.get(0).getWordCount() == 5);
	}

	@Test
	public void testCountWordsInSentencesForText_validMultipleSentences() {
		final String inputText = "This is     a test sentence. Is this the last sentence? No there is a third! ";
		final List<SentenceWordCount> result = countsCalculator.countWordsInSentencesForText(inputText);

		assertNotNull(result);
		assertTrue(result.size() == 3);
		assertTrue(result.get(1).getWordCount() == 5);
		assertTrue(result.get(2).getWordCount() == 5);
	}

	@Test(expected = RuntimeException.class)
	public void testCountWordsInSentencesForText_nullInput() {
		final String inputText = null;
		final List<SentenceWordCount> result = countsCalculator.countWordsInSentencesForText(inputText);
	}

	@Test
	public void testCountWordsInSentence_validSentence() {
		final String inputText = "This is a test sentence";
		final SentenceWordCount result = countsCalculator.countWordsInSentence(inputText);

		assertNotNull(result);
		assertTrue(result.getSentence().equals(inputText));
		assertTrue(result.getWordCount() == 5);
	}

	@Test
	public void testCountWordsInSentence_trailingSPaces() {
		final String inputText = "                    This is a test sentence              ";
		final SentenceWordCount result = countsCalculator.countWordsInSentence(inputText);

		assertNotNull(result);
		assertTrue(result.getSentence().equals(inputText));
		assertTrue(result.getWordCount() == 5);
	}

	@Test
	public void testCountWordsInSentence_onlyLowerCase() {
		final String inputText = "aaaaaaa";
		final List<AlphabetCount> result = countsCalculator.countAlphabetsInText(inputText);

		assertNotNull(result);
		assertTrue(result.get(0).getAlphabetCount() == 7);
	}

	@Test
	public void testCountWordsInSentence_mixedCase() {
		final String inputText = "aaaaaAA";
		final List<AlphabetCount> result = countsCalculator.countAlphabetsInText(inputText);

		assertNotNull(result);
		assertTrue(result.get(0).getAlphabetCount() == 7);
	}

	@Test(expected = RuntimeException.class)
	public void testCountWordsInSentence_emptyInput() {
		final String inputText = "";
		final List<AlphabetCount> result = countsCalculator.countAlphabetsInText(inputText);
	}
}
