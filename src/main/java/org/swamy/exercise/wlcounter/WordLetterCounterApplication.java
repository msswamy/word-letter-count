package org.swamy.exercise.wlcounter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WordLetterCounterApplication {

	public static void main(String[] args) {
		SpringApplication.run(WordLetterCounterApplication.class, args);
	}
}
